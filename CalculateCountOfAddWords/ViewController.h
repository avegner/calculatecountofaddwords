//
//  ViewController.h
//  CalculateCountOfAddWords
//
//  Created by Alex on 10/11/15.
//  Copyright © 2015 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *startAction;
@property (weak, nonatomic) IBOutlet UIProgressView *progressField;
@property (weak, nonatomic) IBOutlet UILabel *countField;



@end

