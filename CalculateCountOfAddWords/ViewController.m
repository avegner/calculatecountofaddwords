//
//  ViewController.m
//  CalculateCountOfAddWords
//
//  Created by Alex on 10/11/15.
//  Copyright © 2015 Alex. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"

@interface ViewController ()

@property (strong, nonatomic) NSArray *urls;
@property (strong, atomic) NSMutableArray *content;

@property (strong, nonatomic) NSOperationQueue *operationQueue;

@property (assign, atomic) NSUInteger coutnOfAddWord;
@property (assign, atomic) NSInteger progressValue;
- (IBAction)startAction:(id)sender;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _operationQueue = [[NSOperationQueue alloc] init];
    [_operationQueue setMaxConcurrentOperationCount:5];    
    [self loadUrls];
}

- (void)loadUrls {
    NSString* path = [[NSBundle mainBundle] pathForResource:@"urls"
                                                     ofType:@"txt"];
    
    NSString*  text = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    
    _urls = [text componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\n"]];
}

- (void)loadContent {
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFCompoundResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    for(int i = 0; i < [_urls count]; i++) {
        self.progressValue = (i+1) / [_urls count]  * 100;
        NSString *url = _urls[i];
        [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSString *response = [[NSString alloc] initWithData:responseObject encoding:1];
            [_operationQueue addOperationWithBlock:^ {
                NSUInteger length = [response length];
                NSRange range = NSMakeRange(0, length);
                while(range.location != NSNotFound)
                {
                    range = [response rangeOfString: @"add" options:0 range:range];
                    if(range.location != NSNotFound)
                    {
                        range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
                        self.coutnOfAddWord++;
                        NSLog(@"Count: %ld", _coutnOfAddWord);
                    }
                }
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Main thread work (UI usually)
                    [self updateProgress];
                    if (_progressValue == 100) {
                        [[self startButton]setEnabled:TRUE];
                    }
                }];
            }];
            NSLog(@"JSON: %@", response);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }
}


- (void)updateProgress {
    [self progressField].progress = _progressValue / 100.0f;
    [self.countField setText:[[NSString alloc]initWithFormat:@"%ld", _coutnOfAddWord]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startAction:(id)sender {
    [[self startButton]setEnabled:FALSE];
    _coutnOfAddWord = 0;
    _progressValue = 0;
    [self updateProgress];
    [self loadContent];
}

@end
