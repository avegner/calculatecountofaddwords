//
//  AppDelegate.h
//  CalculateCountOfAddWords
//
//  Created by Alex on 10/11/15.
//  Copyright © 2015 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

